#!/bin/bash 
[[ -z "$TMPDIR" ]] && TMPDIR=/tmp
[[ -z "$TOR_SOCKS" ]] && TOR_SOCKS=127.0.0.1:9050

timeout 20 curl -s whatismyip.akamai.com &

(sleep 90 ;while (true);do 
    for iface in $(cat  /proc/net/if_inet6|sed 's/ \+/ /g'|cut -d" " -f6|grep -v lo|sort -u);do 
    now=$(date +%s)
    then=$(cat ${TMPDIR}/.started)
    [[ -z "$then" ]] && then=$now
    echo "UPTIME: " $(($now-$then))" | TRAFFIC:"$iface" |";ifconfig $iface |grep -e ytes |tr -d '\n';echo;done  ;sleep 600;done ) &


sed 's~SERVICE_NGINX_OPTS$~\0 2>\&1 |grep -v -e "GET /healthcheck HTTP/1.1. 200"~g' /opt/docker/bin/service.d/nginx.sh -i

sed 's~SERVICE_SYSLOG_OPTS$~\0 2>\&1 |grep -v -e "This port is not an HTTP"~g' /opt/docker/bin/service.d/syslog-ng.sh -i
#sed 's~access_log.\+~access_log /dev/null;~g' -i /etc/nginx/nginx.conf /opt/docker/etc/nginx/vhost.common.d/*
sed 's~access_log.\+~~g' -i /etc/nginx/nginx.conf /opt/docker/etc/nginx/vhost.common.d/*

echo "ICBsb2dfZm9ybWF0ICBtYWluICAnWyR0aW1lX2xvY2FsXSAkcmVtb3RlX2FkZHIgIiRyZXF1ZXN0IiAnCiAgICAgICAgICAgICAgICAgICAgICAnJHN0YXR1cyAkYm9keV9ieXRlc19zZW50ICIkaHR0cF9yZWZlcmVyIiAnCiAgICAgICAgICAgICAgICAgICAgICAnIiRodHRwX3VzZXJfYWdlbnQiICIkaHR0cF94X2ZvcndhcmRlZF9mb3IiICRyZW1vdGVfdXNlcic7Cg=="|base64 -d > /etc/nginx/conf.d/01-logformat.conf
echo '

map $request_method $methloggable {
       # volatile;
#default       $statusloggable;
default       0;
POST          1;
OPTIONS       1;
GET           1;
PUT           1;

}
map $http_user_agent $ualoggable {
       # volatile;

~Pingdom 0;
~Amazon-Route53 0;
~kube-check 0;
default $methloggable;

}
map $status $statusloggable {
        #volatile;
#    ~^[36789]  0;
    200         0;
    301         0;
    302         0;
    499         0; ## client disconnected → HTTP/1.1" 499 → uptime monitors will quit on first keyword and produce tons of 499
    default    $ualoggable;
}
map $request_uri $urlregxloggable {
      #  volatile;
    (.*?)healthcheck(.*?) 0;
    (.*?)ip_info(.*?)     1;
    default $statusloggable;
    }
map $request_uri $loggable {
  /healthcheck.html  0;
  /healthcheck       0;
  /healthcheck_full  1;
  /ip_info           1;
  /ip_info_json      1;
  default $urlregxloggable;
}

map $status $errorloggable {
    499        0; ## client disconnected → HTTP/1.1" 499 → uptime monitors will quit on first keyword and produce tons of 499
    default    1;
}
access_log    /docker.stdout main if=$loggable;
#error_log    /docker.stderr warn if=$errorloggable;
error_log    /docker.stderr warn ;
' > /etc/nginx/conf.d/02-loggable.conf

FPM_PM_MAX_CHILDREN=96
echo "pm.max_children = ${FPM_PM_MAX_CHILDREN}" >> /opt/docker/etc/php/fpm/pool.d/application.conf


#echo '
#access_log    /docker.stdout main if=$loggable;
#error_log    /docker.stderr warn;' > /opt/docker/etc/nginx/vhost.common.d/10-log.conf
echo > /opt/docker/etc/nginx/vhost.common.d/10-log.conf

echo '
location /nginx_status {
 	stub_status;
 	allow 127.0.0.1;	#only allow requests from localhost
 	deny all;		#deny all other hosts	
 }
'  > /opt/docker/etc/nginx/vhost.common.d/01-location-status.conf

echo 'location / {
    set_real_ip_from    192.168.0.0/16; #IP Address of slave LB
    set_real_ip_from    10.0.0.0/8; #IP Address of slave LB
    set_real_ip_from    172.16.0.0/20; #IP Address of slave LB
    set_real_ip_from    127.0.0.0/8; #IP Address of slave LB
    real_ip_header      X-Forwarded-For;
    real_ip_recursive on;
    try_files $uri $uri/ /index.php?$query_string;
#    access_log   /dev/stdout main if=$loggable;
    #access_log /dev/null;
} ' > /opt/docker/etc/nginx/vhost.common.d/10-location-root.conf

## see https://serverfault.com/questions/759762/how-to-stop-nginx-301-auto-redirect-when-trailing-slash-is-not-in-uri/812461#812461



echo '
location ~* /healthcheck_full {
    set_real_ip_from    192.168.0.0/16; #IP Address of slave LB
    set_real_ip_from    10.0.0.0/8; #IP Address of slave LB
    set_real_ip_from    172.16.0.0/20; #IP Address of slave LB
    set_real_ip_from    127.0.0.0/8; #IP Address of slave LB
    real_ip_header      X-Forwarded-For;
    real_ip_recursive on;
    try_files  $uri $uri/ /index.php?$query_string;
    access_log /dev/null ;
} 
' > /opt/docker/etc/nginx/vhost.common.d/03.1-location-healthcheck_full.conf

echo '
location ~* /healthcheck {
    set_real_ip_from    192.168.0.0/16; #IP Address of slave LB
    set_real_ip_from    10.0.0.0/8; #IP Address of slave LB
    set_real_ip_from    172.16.0.0/20; #IP Address of slave LB
    set_real_ip_from    127.0.0.0/8; #IP Address of slave LB
    real_ip_header      X-Forwarded-For;
    real_ip_recursive on;
    #try_files  $uri $uri/ /index.php?$query_string;

    return 200 "OK";
    # because default content-type is application/octet-stream, browser will offer to "save the file"... if you want to see reply in browser, uncomment next line 
    add_header Content-Type text/plain;
    access_log /dev/null ;
} 
' > /opt/docker/etc/nginx/vhost.common.d/03.2-location-healthcheck.conf


### endpoints

(cd /scripts ; ls -1 |grep sh$| sed 's/\.sh//g' |grep -v ^$)|grep -v ip_info |while read endpoint;do 


echo '
location ~* /'$endpoint' {
    root /app/;
    set_real_ip_from    192.168.0.0/16; #IP Address of slave LB
    set_real_ip_from    10.0.0.0/8; #IP Address of slave LB
    set_real_ip_from    172.16.0.0/20; #IP Address of slave LB
    set_real_ip_from    127.0.0.0/8; #IP Address of slave LB
    real_ip_header      X-Forwarded-For;
    ## auto generated /'$endpoint' config
    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME   /app/index.php;
#    try_files  /index.php?$query_string $uri $uri/;
    try_files  $uri $uri/ /index.php?$query_string;
    #access_log /dev/null ;
    real_ip_recursive on;
    access_log    /docker.stdout main if=$loggable;
}' > /opt/docker/etc/nginx/vhost.common.d/05-location-${endpoint}.conf


done

(cd /scripts ; ls -1 |grep sh$| sed 's/\.sh//g' |grep -v ^$)|grep   ip_info |while read endpoint;do 


echo '
location ~* /'$endpoint' {
    root /app/;
    set_real_ip_from    192.168.0.0/16; #IP Address of slave LB
    set_real_ip_from    10.0.0.0/8; #IP Address of slave LB
    set_real_ip_from    172.16.0.0/20; #IP Address of slave LB
    set_real_ip_from    127.0.0.0/8; #IP Address of slave LB
    real_ip_header      X-Forwarded-For;
    ## auto generated /'$endpoint' config
    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME   /app/index.php;
#    try_files  /index.php?$query_string $uri $uri/;
    try_files  $uri $uri/ /index.php?$query_string;
    #access_log /dev/null ;
    real_ip_recursive on;
    access_log    /docker.stdout main if=$loggable;
}' > /opt/docker/etc/nginx/vhost.common.d/05-location-${endpoint}.conf


done


#echo "Log notice stderr" >> /etc/tor/torrc
(
    echo "WarnUnsafeSocks 0"
    echo "WarnPlaintextPorts 1"
    echo "Log warn-err stderr" )>> /etc/tor/torrc

#/etc/init.d/tor start & 
( tor -f /etc/tor/torrc 2>&1 |grep -v -e "This port is not an HTTP"  ) &

#/etc/init.d/dnsmasq start &
dnsmasq -f -d --cache-size=32768 --no-negcache --min-cache-ttl=0 --max-cache-ttl=1800 --all-servers & 

## 
echo nameserver 127.0.0.1 > /etc/resolv.conf &
sed 's~access.log =.\+~access.log = /dev/null~g' -i  /etc/php/*/fpm/pool.d/application.conf /opt/docker/etc/php/fpm/pool.d/application.conf
cp -aurv /scripts/ ${TMPDIR}/scripts;chmod -R ugo-w ${TMPDIR}/scripts ;

sysctl net.ipv6.conf.all.disable_ipv6=0 & 

date > ${TMPDIR}/.started_human
date +%s >${TMPDIR}/.started


##ipv6 with tun through teredo
test -e /dev/net/tun && ( 
    sysctl net.ipv6.conf.all.disable_ipv6=0 || true && /etc/init.d/miredo start 
    sleep 120 ; ip -6 a|grep -q global || /etc/init.d/miredo restart  )  &
 

 ##updaters
( while (true);do 
    sleep 86400;
    apt-get -y update;apt-get -y install ca-certificates
  done
) &
( 
  while (true);do 
    sleep 14400;
    ( cd ${TMPDIR}/scripts/sshscan_evict ; git pull 2>&1 ) |tr -d '\n'
    ( cd ${TMPDIR}/scripts/sshscan       ; git pull 2>&1 ) |tr -d '\n'
    rawnew=$(curl -s  https://svn.nmap.org/nmap/scripts/ssl-enum-ciphers.nse)
    suma=$(cat /scripts/ssl-enum-ciphers.nse |md5sum|cut -d" " -f1 )
    sumb=$(echo "$rawnew" |md5sum|cut -d" " -f1)
    [[ "$suma" = "$sumb" ]] || {
        echo "$rawnew"|grep -q "least strength" && {
            echo "$rawnew"|tee -a /scripts/ssl-enum-ciphers.nse ${TMPDIR}/scripts/ssl-enum-ciphers.nse
        }
    echo -n ; } 

 done
 ##end updater
) &

( sleep 30;
  MYTOKEN=$(echo "$TOKENS"|cut -d":" -f1)
  [[ -z "$MYTOKEN" ]] && MYTOKEN="__________________"
  echo "SELF HEALTHCHECKS " ; for url in healthcheck healthcheck_full myuptime ;do echo "###uptime test /$url ### "; curl -u selfcheck:$MYTOKEN -s "127.0.0.1/$url" ;done 
) &


test -e /usr/bin/myuptime && /usr/bin/myuptime
test -e /etc/.buildtime && echo "  BUILDŧ : "$(cat /etc/.buildtime)
echo "INIT DONE: .. starting"
/usr/bin/python /usr/bin/supervisord -c /opt/docker/etc/supervisor.conf --logfile /dev/null --pidfile /dev/null --user root 2>&1 |grep -v -e '"GET /healthcheck HTTP/1.1" 200' -e "php-fpm:access" -e "This port is not an HTTP" 


wait;fg

