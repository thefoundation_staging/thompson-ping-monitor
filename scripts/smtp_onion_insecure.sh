#!/bin/bash 
[[ -z "$TMPDIR" ]] && TMPDIR=/tmp
[[ -z "$TOR_SOCKS" ]] && TOR_SOCKS=127.0.0.1:9050

target=$1
#SPORT=${TOR_SOCKS/*:/}
#SHOST=${TOR_SOCKS/:*/}

hostbuf=$( curl --retry 2 --retry-delay 1 --retry-max-time 235 --socks5-hostname "${TOR_SOCKS}" "smtp://${target}" -kLv 2>&1  )

(
    echo "$hostbuf" | grep -e 250-ETRN -e 250-8BITMIME -e 250-SMTPUTF8 -e "SMTP READY" -e 250-PIPELINING -e "250 STARTTLS" -e 250-STARTTLS -e "250-"${target/:*/} -e 250-DSN -q && echo OK 2>&1 
    echo "$hostbuf" | grep -e 250-ETRN -e 250-8BITMIME -e 250-SMTPUTF8 -e "SMTP READY" -e 250-PIPELINING -e "250 STARTTLS" -e 250-STARTTLS -e "250-"${target/:*/} -e 250-DSN -q || ( echo FAIL: ;echo "$hostbuf")  2>&1 
) 2>&1 
