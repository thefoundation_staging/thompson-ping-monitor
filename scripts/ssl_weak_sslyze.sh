#!/bin/bash 
[[ -z "$TMPDIR" ]] && TMPDIR=/tmp
[[ -z "$TOR_SOCKS" ]] && TOR_SOCKS=127.0.0.1:9050

target=$1
cd /tmp/scripts/testssl
torsocks python3 -m sslyze $target 2>&1 


