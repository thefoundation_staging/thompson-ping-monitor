#!/bin/bash 
[[ -z "$TMPDIR" ]] && TMPDIR=/tmp
[[ -z "$TOR_SOCKS" ]] && TOR_SOCKS=127.0.0.1:9050

target=$1
#SPORT=${TOR_SOCKS/*:/}
#SHOST=${TOR_SOCKS/:*/}

test -e ${TMPDIR}/.ip_json_cache || mkdir ${TMPDIR}/.ip_json_cache

find "${TMPDIR}/.ip_json_cache/" -mmin +120 -type f -delete
tname_flat=${target/\./_}
tname_flat=${tname_flat/:/_}

test -e "${TMPDIR}/.ip_json_cache/$tname_flat" && {
    grep -q '"ip"' "${TMPDIR}/.ip_json_cache/$tname_flat" && {
        cat        "${TMPDIR}/.ip_json_cache/$tname_flat"
        exit
    }
}

myurls=$(
    echo "https://ipinfo.io/$target"
    echo "https://ifconfig.co/json?ip=$target"
    echo "https://ipapi.co/$target/json/"
    echo "http://ip-api.com/json/$target"
    echo "http://ipwho.is/$target"
)

failed=yes

##2 random rounds , also shuf seems to be more random on at least 2 elements
for checkurl in $( (echo "$myurls";echo "$myurls"|tac )|shuf);do
    echo "$failed"|grep ^yes$ -q && {
           res=$(curl -s --retry 1 --retry-delay 1 --retry-max-time 23 --socks5-hostname "${TOR_SOCKS}" "$checkurl" |sed 's~https://ipinfo.io/missingauth~'"$checkurl"'~g')
           echo "$res" | grep  -q -e "Rate limit exceeded" -e "hit the daily limit for the unauthenticated API" && failed=yes
           echo "$checkurl"|grep -q "/ip-api.com/json" && res=$(echo "$res"|sed 's/"query":/"ip":/g'|sed 's/","/",\n    "/g')
           echo "$checkurl"|grep -q "ipwho.is/"        && res=$(echo "$res"|sed 's/","/",\n    "/g')
           echo "$res" | grep  -q '"ip":' && failed=no
    }
    echo "$failed"|grep -q ^no$ && {
        echo "$res"|jq . |tee "${TMPDIR}/.ip_json_cache/$tname_flat" && exit 0 
    }
done

echo '{"status": "FAIL","reason": "NO_RESULT"}'
