#!/bin/bash
[[ -z "$TMPDIR" ]] && TMPDIR=/tmp

ip=$1
[[ -z "$ip" ]] && echo "FAIL:EMPTY_IP $ip"
[[ -z "$ip" ]] && exit 1

( echo "$ip"|grep -q -e ":" -e '\.') ||  echo "FAIL:NON_IP4_OR_IP6 $ip"
( echo "$ip"|grep -q -e ":" -e '\.') ||  exit 2
(echo "${ip}"|grep -q -e ^127\. -e ^10\. -e ^192\.168\. -e '^172.1[6-9]\.' -e '^172.2[0-9]\.' ) && echo "FAIL:PRIV_NET $ip"
(echo "${ip}"|grep -q -e ^127\. -e ^10\. -e ^192\.168\. -e '^172.1[6-9]\.' -e '^172.2[0-9]\.' ) && exit 3


(echo "${ip}"|grep -q -e ^127\. -e ^10\. -e ^192\.168\. -e '^172.1[6-9]\.' -e '^172.2[0-9]\.' ) || ( echo "$ip"|grep  -v ":" | grep -q '\.') &&  (
        ##ip was found
          #echo -en '\t';
          target=$(echo $ip|awk -F. '{print $4"."$3"." $2"."$1}');
          (  timeout 10 host -t txt $target.sa.senderbase.org            127.0.0.1   || timeout 5 host -t txt $target.sa.senderbase.org 1.1.1.1 ) |sed 's/|/\n/g;s/senderbase.org descriptive text.\+|//g'|grep -e ^1= -e 20= -e 53= -e 54= -e 55= -e 50= |sed 's/50=/CITY=/g;s/54=/LAT=/g;s/55=/LON=/g;s/53=/CC=/g;s/^20=/0000=/g;s/^1=/0005=/g;s/^26=/0002=/g;s/^47=/0001=/g'|sort|grep -v 0000=|sed 's/0000=//g;s/0002=/SCORE_DOMAIN=/g;s/0001=/SCORE_IP=/g;s/0005=/ORG=/g;s/^/|/g'|tr -d '\n';
          echo "|"$( ( timeout 10 host    $target.score.senderscore.com  127.0.0.1   || timeout 5 host    $target.score.senderscore.com 1.1.1.1 ) |grep 127.0.4.|grep -v NXDOMAIN|sed 's/.\+127\.0\.4\.//g;s/^/senderscore: /g' 2>&1 )"|" ;

        echo "|OK|IP=${ip}|"
        ) |sed 's/^\( \|\t\)\+//g;s/|\+/|/g'
        ## end ip was found
#(echo "${ip}"|grep -q -e ^127\. -e ^10.\. -e ^192\.168\. -e '^172.1[6-9]\.' -e '^172.2[0-9]\.' ) && echo "FAIL:PRIV_NET $ip"
#( echo "$ip"|grep -q -e ":" -e '\.') ||  echo "FAIL:NON_IP4_OR_IP6 $ip"

#[[ -z "$ip" ]] && echo "FAIL:EMPTY_IP $ip"
