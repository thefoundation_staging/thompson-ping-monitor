#!/bin/bash 
[[ -z "$TMPDIR" ]] && TMPDIR=/tmp
[[ -z "$TOR_SOCKS" ]] && TOR_SOCKS=127.0.0.1:9050

target=$1
#SPORT=${TOR_SOCKS/*:/}
#SHOST=${TOR_SOCKS/:*/}
hostbuf=$(curl --retry 2 --retry-delay 1 --retry-max-time 235 --socks5-hostname "${TOR_SOCKS}" "ldaps://${target}" 2>&1 )

(
    echo "$hostbuf" | grep DN: -q && echo OK 2>&1 
    echo "$hostbuf" | grep DN: -q || ( echo FAIL: ;echo "$hostbuf")  2>&1 
) 2>&1 

