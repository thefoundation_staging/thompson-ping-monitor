#!/bin/bash 
[[ -z "$TMPDIR" ]] && TMPDIR=/tmp
[[ -z "$TOR_SOCKS" ]] && TOR_SOCKS=127.0.0.1:9050

target=$1

echo "$target"|grep ":" || targport=22

echo "$target"|grep ":" && {
    tmpport=${target/*://}
    tmphost=${target/:*//}
    target=$tmphost
    targport=$tmpport
}

[[ -z "$targport" ]] && targport=22

SPORT=${TOR_SOCKS/*:/}
SHOST=${TOR_SOCKS/:*/}
( torsocks ssh-keyscan -p "$targport" ${target} 2>&1  && echo OK 2>&1 ) 2>&1 


