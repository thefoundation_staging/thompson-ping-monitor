#!/bin/bash 
[[ -z "$TMPDIR" ]] && TMPDIR=/tmp
[[ -z "$TOR_SOCKS" ]] && TOR_SOCKS=127.0.0.1:9050

test -e ${TMPDIR}/.sshscan || mkdir ${TMPDIR}/.sshscan

find ${TMPDIR}/.sshscan/ -type f -mtime +1 -delete & 

target=$1
echo "$target"|grep -q ":" || target=${target}":22"
thost=${target/:*/}
tport=${target/*:/}
#echo "torsocks python3 sshscan.py --target=${thost}":"${tport}"
sleep 0.2 

OUTFILE="${TMPDIR}/.sshscan/scan_evict_"$( echo "$target"|md5sum|cut -d" " -f1)
( test -e /$OUTFILE || { 
( cd ${TMPDIR}/scripts/sshscan_evict; torsocks python3 sshscan.py --target=${thost}":"${tport} |tail -n+9|grep -v ^$ ) &>$OUTFILE
echo -n ; } ; ) & 


OUTFILE_B="${TMPDIR}/.sshscan/scan_"$( echo "$target"|md5sum|cut -d" " -f1)

( test -e /$OUTFILE_B || { 
( cd ${TMPDIR}/scripts/sshscan;torsocks python3 sshscan.py --target=${thost} --port=${tport} |tail -n+9|grep -v ^$ )  &>$OUTFILE_B 
echo -n ; } ; 
) & 

wait

one_ok=no
grep '\[Weak\]' "$OUTFILE_B" -q &&  sed 's/^/sshscan      .py: /' "$OUTFILE_B"
grep '\[Weak\]' "$OUTFILE_B" -q || one_ok=yes
grep -e "Error while connecting" -e "Failed to connect" "$OUTFILE_B" && one_ok=no

two_ok=no
grep 'the following weak' "$OUTFILE" -q &&  sed 's/^/sshscan_evict.py: /' "$OUTFILE"
grep 'the following weak' "$OUTFILE" -q || two_ok=yes
grep -e "Error while connecting" -e "Failed to connect" "$OUTFILE" && two_ok=no

[[ "$one_ok" = "yes" ]] && [[ "$two_ok" = "yes" ]] && echo "OK"
