#!/bin/bash 
[[ -z "$TMPDIR" ]] && TMPDIR=/tmp
[[ -z "$TOR_SOCKS" ]] && TOR_SOCKS=127.0.0.1:9050

target=$1
cd /tmp/scripts/testssl
/bin/bash testssl.sh --openssl /tmp/scripts/openssl --quiet --sneaky --connect-timeout 23 --openssl-timeout 42 --phone-out --vulnerable --protocols  $target 2>&1 


