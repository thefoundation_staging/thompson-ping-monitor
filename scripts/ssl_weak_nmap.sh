#!/bin/bash 
[[ -z "$TMPDIR" ]] && TMPDIR=/tmp
[[ -z "$TOR_SOCKS" ]] && TOR_SOCKS=127.0.0.1:9050

target=$1
echo "$target"|grep -q ":" || target=${target}":443"
thost=${target/:*/}
tport=${target/*:/}
#echo "torsocks python3 sshscan.py --target=${thost}":"${tport}"
sleep 0.2 
#res=$( cd /scripts; torsocks nmap -sT -PN -n  --unprivileged --script ssl-enum-ciphers -p $tport $thost 2>&1 )
res=$( cd /scripts; time torsocks nmap -sT -PN -n                  --script ssl-enum-ciphers -p $tport $thost 2>&1 )

echo "$res"|grep "least strength" -q || (echo "FAIL: DID_NOT_RUN";echo "###";cd /scripts/;echo "$res";which nmap ;ls -1 |grep nse$ ;which torsocks
                                        now=$(date +%s)
                                        then=$(cat ${TMPDIR}/.started)
                                        echo "UPTIME: " $(($now-$then))
                                        echo "RESCAN:"
                                        echo pc
                                        time proxychains nmap -sT -PN -n  --script ssl-enum-ciphers -p $tport $thost 2>&1 
                                        )
echo "$res"|grep "least strength" -q || exit 1

echo "$res"|grep -q -e "least strength: C" -e "least strength: D" && (
                                             echo "$res";echo "######";echo "FAIL:LOW_SSL_GRADE:";echo "######"; )

echo "$res"|grep -q  "least strength: B" && (echo "$res";echo "######";echo "OK:MEDIUM_GRADE:";echo "######"; )
echo "$res"|grep -q  "least strength: A" && (echo "$res";echo "######";echo "OK:";echo "######")
exit 0

