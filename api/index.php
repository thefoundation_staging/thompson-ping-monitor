<?php

//functions
function get_target() {
  $target=false;
  if (!isset($_SERVER['PHP_AUTH_USER'])) {
    if(isset($_POST) && isset($_POST['X-THOMPSON-TARGET'])) {
      $target=$_POST['X-THOMPSON-TARGET'];
    }
    if(isset($_GET) && isset($_GET['X-THOMPSON-TARGET'])) {
      $target=$_GET['X-THOMPSON-TARGET'];
    }
    if(isset($_POST) && isset($_POST['TARGET'])) {
      $target=$_POST['TARGET'];
    }
    if(isset($_GET) && isset($_GET['TARGET'])) {
      $target=$_GET['TARGET'];
    }
    if (!empty($_SERVER['HTTP_TARGET']))
    {  $target=$_SERVER['HTTP_TARGET'];    }
  } else {
    $target=$_SERVER['PHP_AUTH_USER'];
  }
if (strpos($target, '=base64=') === 0) {
   $target=base64_decode(preg_replace('/^=base64=/', '', $target));$target=trim(preg_replace('/\s\s+/', ' ', $target));
}
return $target;
}

function set_target($target) {
try {
  if(isset($_GET) ) {
    $_GET['X-THOMPSON-TARGET']=$target;
  }
  if(isset($_POST) ) {
    $_POST['X-THOMPSON-TARGET']=$target;
  }
  if(isset($_POST) ) {
    $_POST['TARGET']=$target;
  }
  if(isset($_GET) ) {
    $_GET['TARGET']=$target;
  }
} catch (Exception $e) {
  // echo smth
  }
}


function verify_onion($addr) {
$str=substr($addr, 0, strpos($addr, ".onion"));
//print($str);print(" LEN: ".strlen($str))."\n";
if((strlen($str) == 56) || (strlen($str) == 16)) {
  return true;
} else {
  return false;
}
}


function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))             //check ip from shared internet
    {      $ip=$_SERVER['HTTP_CLIENT_IP'];    }
    elseif (!empty($_SERVER['X_REAL_IP']))   //to check ip is passed from proxy nginx
    {      $ip=$_SERVER['X_REAL_IP'];    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is passed from proxy
    {      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];    }
    else
    {      $ip=$_SERVER['REMOTE_ADDR'];    }            //both missing , use remote address
    //overwrites
    if (!empty($_SERVER['HTTP_CF_CONNECTING_IP']))   //to check ip is passed from proxy cloudflare
    {      $ip=$_SERVER['HTTP_CF_CONNECTING_IP'];    }
    if (!empty($_SERVER['HTTP_TRUE_CLIENT_IP']))   //to check ip is passed from proxy cloudflare w true client ip
    {      $ip=$_SERVER['HTTP_TRUE_CLIENT_IP'];    }
    return $ip;
}

function ping_ipv6() {
$target=get_target();
$output=array();
$pingCount=3;

$cmd="ping6 -c 3 -i 0.3 -W 6 ".$target." |grep -e statistics -e loss  -e rtt ";
$output=shell_exec($cmd);
file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");

//print(json_encode($output,true));

             //if (isset($_GET['order_id']) && $_GET['order_id']!="") {
              $file = '/tmp/POSTDUMP.json';
              //$content = json_encode($_SERVER,true);
              $content = json_encode($output,true);
$pingLineRegex = $re = "/--- (.+) ping statistics ---\\n([0-9]+) packets transmitted, ([0-9]+) received, ([0-9\\.]+)% packet loss, time ([0-9]+)ms\\nrtt min\\/avg\\/max\\/mdev = ([0-9\\.]+)\\/([0-9\\.]+)\\/([0-9\\.]+)\\/([0-9\\.]+) ms/";
preg_match_all($pingLineRegex, $output, $pingResultMatches);

if (isset($pingResultMatches[1][0]))
{
          $pingStatistics = array(
           'host' => $pingResultMatches[1][0],
           'tx' => $pingResultMatches[2][0],
           'rx' => $pingResultMatches[3][0],
           'loss' => $pingResultMatches[4][0],
           'time' => $pingResultMatches[5][0],
           'min' => $pingResultMatches[6][0],
           'avg' => $pingResultMatches[7][0],
           'max' => $pingResultMatches[8][0],
           'mdev' => $pingResultMatches[9][0],
          );

          if ( isset($pingStatistics['host']) && $pingStatistics['loss'] < 34 ) {
              //ping OK
              header("Content-Type:application/json");
              http_response_code(200);
              print(json_encode($pingStatistics,true));

              //var_dump($_GET);
              //if (isset($_GET['order_id']) && $_GET['order_id']!="") {
              $file = '/tmp/POSTDUMP.json';
              //$content = json_encode($_SERVER,true);
              $content = json_encode($pingStatistics,true);
              file_put_contents($file, $content);
          } else {
              $pingStatistics['FAILED']='LOSS';
              header("Content-Type:application/json");
              http_response_code(503);
              print(json_encode($pingStatistics,true));
          }
  } else  {
              header("Content-Type:application/json");
              http_response_code(503);
              file_put_contents($file, $content);
          }
}

function imap_ipv6() {
//$target=$_SERVER['PHP_AUTH_USER'];
$target=get_target();
$output=array();
$pingCount=3;

$output=array();
$pingCount=3;
$cmd='curl -6 -kLv imap://'.$target.' 2>&1  |grep IMAP|grep STARTTLS -q && echo OK';

file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
$up=false;


$output=shell_exec($cmd);



if (strpos($output, 'OK') !== false) {
  header("Content-Type:application/json");
  http_response_code(200);
  echo $output;
  //print(json_encode($output,true));

} else {
  header("Content-Type:application/json");
  echo 'FAIL:'.$output;
  http_response_code(503);
  file_put_contents($file, $content);
  }
}

function imaps_ipv6() {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
//$pingCount=3;
  $curlext="";
  if(strpos($target, ".onion" ) !== false ) { $curlext=" --socks5-hostname 127.0.0.1:9050 " ; }
  $cmd='curl -6 '.$curlext.' -Lkv imaps://'.$target.' 2>&1  |grep IMAP -q && echo OK';
  $output=shell_exec($cmd); file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");

if (strpos($output, 'OK') !== false) {
  header("Content-Type:application/json");
  http_response_code(200);
  echo $output;
  //print(json_encode($output,true));

} else {
  header("Content-Type:application/json");
  echo 'FAIL:'.$output;
  http_response_code(503);
   $file = '/tmp/POSTDUMP.json';
   $content = json_encode($output,true);
  file_put_contents($file, $content);
  }
}

function imap_onion_ssl() {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
  //$pingCount=3;
  $cmd='curl -6 -Lkv --socks5-hostname 127.0.0.1:9050 imaps://'.$target.' 2>&1  |grep IMAP -q && echo OK';
  $output=shell_exec($cmd); file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");

  if (strpos($output, 'OK') !== false) {
    header("Content-Type:application/json");
    http_response_code(200);
    echo $output;
    //print(json_encode($output,true));
} else {
    echo 'FAIL:'.$output;
    header("Content-Type:application/json");
    http_response_code(503);
   $file = '/tmp/POSTDUMP.json';
   $content = json_encode($output,true);
  file_put_contents($file, $content);
  }
}




function imap_onion_insecure() {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
  $pingCount=3;
  $cmd='curl -6 -kLv --socks5-hostname 127.0.0.1:9050 imap://'.$target.' 2>&1  |grep IMAP -q && echo OK';
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);
 if (strpos($output, 'OK') !== false) {
  header("Content-Type:application/json");
  http_response_code(200);
  echo $output;
  //print(json_encode($output,true));

} else {
  echo 'FAIL:'.$output;
  header("Content-Type:application/json");
  http_response_code(503);
  $file = '/tmp/POSTDUMP.json';
  $content = json_encode($output,true);
 file_put_contents($file, $content);
 }
}


function imap_onion_tls() {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
  $pingCount=3;
  $cmd='curl -6 -kLv --socks5-hostname 127.0.0.1:9050 imap://'.$target.' --ssl-reqd 2>&1  |grep IMAP|grep STARTTLS -q && echo OK';
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);
 if (strpos($output, 'OK') !== false) {
  header("Content-Type:application/json");
  http_response_code(200);
  echo $output;
  //print(json_encode($output,true));

} else {
  echo 'FAIL:'.$output;
  header("Content-Type:application/json");
  http_response_code(503);
  $file = '/tmp/POSTDUMP.json';
  $content = json_encode($output,true);
 file_put_contents($file, $content);
 }
}

function ssh_onion() {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
  $cmd='/bin/bash /scripts/ssh_onion.sh '.$target;
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);

  if (strpos($output, 'OK') !== false) {
    header("Content-Type:application/json");
    http_response_code(200);
    echo $output;
    //print(json_encode($output,true));

} else {
    echo 'FAIL:'.$output;
    header("Content-Type:application/json");
    http_response_code(503);

     $file = '/tmp/POSTDUMP.json';
     $content = json_encode($output,true);
    file_put_contents($file, $content);
    }

}
function ssh_weak_onion() {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
  $cmd='/bin/bash /scripts/ssh_weak_onion.sh '.$target;
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);

  if (strpos($output, 'OK') !== false) {
    header("Content-Type:application/json");
    http_response_code(200);
    echo $output;
    //print(json_encode($output,true));

} else {
    echo 'FAIL:'.$output;
    header("Content-Type:application/json");
    http_response_code(503);

     $file = '/tmp/POSTDUMP.json';
     $content = json_encode($output,true);
    file_put_contents($file, $content);
    }

}

function ssl_weak_nmap() {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
  $cmd='/bin/bash /scripts/ssl_weak_nmap.sh '.$target;
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);

  if (strpos($output, 'OK') !== false) {
                header("Content-Type:application/json");
                http_response_code(200);
                echo $output;
                //print(json_encode($output,true));

    } else {
                echo 'FAIL:'.$output;
                header("Content-Type:application/json");
                http_response_code(503);


     $file = '/tmp/POSTDUMP.json';
     $content = json_encode($output,true);
    file_put_contents($file, $content);
    }

}
function ssh_keyscan_onion() {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
  $cmd='/bin/bash /scripts/ssh_keyscan_onion.sh '.$target;
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);

  if (strpos($output, 'OK') !== false) {
                header("Content-Type:application/json");
                http_response_code(200);
                echo $output;
                //print(json_encode($output,true));

    } else {
                echo 'FAIL:'.$output;
                header("Content-Type:application/json");
                http_response_code(503);



     $file = '/tmp/POSTDUMP.json';
     $content = json_encode($output,true);
    file_put_contents($file, $content);
    }

}

function ldaps() {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
  $pingCount=3;
  $output=array();
  $cmd='/bin/bash -c "curl ldaps://'.$target.' | grep DN: -q && echo OK " 2>&1';
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);
  if (strpos($output, 'OK') !== false) {
    header("Content-Type:application/json");
    http_response_code(200);
    echo $output;
    //print(json_encode($output,true));

} else {
    echo 'FAIL:'.$output;
    header("Content-Type:application/json");
    http_response_code(503);
     $file = '/tmp/POSTDUMP.json';
     $content = json_encode($output,true);
    file_put_contents($file, $content);
    }

}



function ssl_weak_testssl() {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
  //$cmd='/bin/bash -c "curl --socks5-hostname 127.0.0.1:9050 ldaps://'.$target.' | grep DN: -q && echo OK " 2>&1';
  $cmd='/bin/bash /scripts/ssl_weak_testssl.sh '.$target;
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);
  if (strpos($output, 'OK') !== false) {
    header("Content-Type:application/json");
    http_response_code(200);
    echo $output;
    //print(json_encode($output,true));

} else {
    echo 'FAIL:'.$output;
    header("Content-Type:application/json");
    http_response_code(503);
     $file = '/tmp/POSTDUMP.json';
     $content = json_encode($output,true);
    file_put_contents($file, $content);
    }

}


  function ssl_weak_sslyze() {
    $target=get_target();
    $output=array();
    //$cmd='/bin/bash -c "curl --socks5-hostname 127.0.0.1:9050 ldaps://'.$target.' | grep DN: -q && echo OK " 2>&1';
    $cmd='/bin/bash /scripts/ssl_weak_sslyze.sh '.$target;
    file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
    $output=shell_exec($cmd);
    if (strpos($output, 'OK') !== false) {
      header("Content-Type:application/json");
      http_response_code(200);
      echo $output;
      //print(json_encode($output,true));

  } else {
      echo 'FAIL:'.$output;
      header("Content-Type:application/json");
      http_response_code(503);
       $file = '/tmp/POSTDUMP.json';
       $content = json_encode($output,true);
      file_put_contents($file, $content);
      }

  }
function ldaps_onion() {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
  //$cmd='/bin/bash -c "curl --socks5-hostname 127.0.0.1:9050 ldaps://'.$target.' | grep DN: -q && echo OK " 2>&1';
  $cmd='/bin/bash /scripts/ldaps_onion.sh '.$target;
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);
  if (strpos($output, 'OK') !== false) {
    header("Content-Type:application/json");
    http_response_code(200);
    echo $output;
    //print(json_encode($output,true));

} else {
    echo 'FAIL:'.$output;
    header("Content-Type:application/json");
    http_response_code(503);
     $file = '/tmp/POSTDUMP.json';
     $content = json_encode($output,true);
    file_put_contents($file, $content);
    }

}


function smtp_onion($variant) {
  $target=get_target();
  if((strpos($target, ".onion") !== false) && !verify_onion($target) ) {
    header("Content-Type:application/json");
    echo 'FAIL:'.$target."FAILED ONION VRFY";
    file_put_contents( "php://stderr","ERR: ".$target."FAILED ONION VRFY" );
    http_response_code(400);
    exit(0);
    }
  $output=array();
  $pingCount=3;
  $output=array();
  //$cmd='/bin/bash -c "curl --socks5-hostname 127.0.0.1:9050 ldaps://'.$target.' | grep DN: -q && echo OK " 2>&1';
  $cmd='/bin/bash /scripts/smtp_onion_'.$variant.'.sh '.$target;
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);
  if (strpos($output, 'OK') !== false) {
    header("Content-Type:application/json");
    http_response_code(200);
    echo $output;
    //print(json_encode($output,true));

} else {
    echo 'FAIL:'.$output;
    header("Content-Type:application/json");
    http_response_code(503);
     $file = '/tmp/POSTDUMP.json';
     $content = json_encode($output,true);
    file_put_contents($file, $content);
    }

}

function ip_info($intarget) {

  if($intarget=="") { $intarget=get_target(); }
  //echo 'INTARGET:'.$intarget;
  $target=$intarget;
  if($target=="")    {   $target=getRealIpAddr();
  }
  $output=array();
  //echo 'SENDTARGET:'.$target;
  $cmd='/bin/bash /scripts/ip_info.sh '.$target;
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);
  if (strpos($output, 'OK') !== false) {
    header("Content-Type:application/json");
    http_response_code(200);
    echo $output;
    //print(json_encode($output,true));

} else {
    echo 'FAIL:'.$output;
    header("Content-Type:application/json");
    http_response_code(503);
     $file = '/tmp/POSTDUMP.json';
     $content = json_encode($output,true);
    file_put_contents($file, $content);
    exit;
    }

}

function ip_info_json($intarget) {

  if($intarget=="") { $intarget=get_target(); }
  //echo 'INTARGET:'.$intarget;
  $target=$intarget;
  if($target=="")    {   $target=getRealIpAddr();
  }
  $output=array();
  //echo 'SENDTARGET:'.$target;
  $cmd='/bin/bash /scripts/ip_info_json.sh '.$target;
  file_put_contents('/dev/shm/thompson.lastcmd', $cmd . "\n");
  $output=shell_exec($cmd);
  if (strpos($output, '"ip":') !== false) {
    header("Content-Type:application/json");
    http_response_code(200);
    echo $output;
    //print(json_encode($output,true));

} else {
    //echo 'FAIL:'.$output;
    header("Content-Type:application/json");
    http_response_code(503);
    echo '{"ip": "'.$target.'","status": "FAIL"}';
     $file = '/tmp/POSTDUMP.json';
     $content = json_encode($output,true);
    file_put_contents($file, $content);
    exit;
    }

}
//

// end func def

// ROUTES #################


if($_SERVER['REQUEST_URI'] == '/healthcheck_full' )
      {
        http_response_code(200);
       // header("Content-Type:application/json");
        $extip = getRealIpAddr() ;
        if($extip=="127.0.0.1") { $extip=""; }
        if($extip=="::1") { $extip=""; }
        //echo "OK - YOUR_IP: ".$extip;
        if($extip!="") { $extip="CLIENT_IP: ".$extip; }
        echo shell_exec('/bin/bash /healthcheck').$extip ;
        exit;
      }

if($_SERVER['REQUEST_URI'] == '/myuptime' )
      {
        http_response_code(200);
        echo shell_exec('/usr/bin/myuptime') ;
        echo shell_exec('echo -n "BUILDTIME: ";cat /etc/.buildtime') ;
        echo shell_exec('curl -s 127.0.0.1/nginx_status') ;
        exit;
      }

if(($_SERVER['REQUEST_URI'] == '/ip_info' )|| (strpos($_SERVER['REQUEST_URI'], '/ip_info?') === 0) ) {
        ip_info("");
        exit;
      }

if(strpos($_SERVER['REQUEST_URI'], '/ip_info/') === 0 ) {
        $str=$_SERVER['REQUEST_URI'];
        $prefix='/ip_info/';
        if (substr($str, 0, strlen($prefix)) == $prefix) {
          $str = substr($str, strlen($prefix));
        }
        //echo 'SET_TARGET:'.$str;
        set_target($str);
        ip_info($str);
        exit;
      }
if(($_SERVER['REQUEST_URI'] == '/ip_info_json' )|| (strpos($_SERVER['REQUEST_URI'], '/ip_info?') === 0) ) {
        ip_info_json("");
        exit;
      }

if(strpos($_SERVER['REQUEST_URI'], '/ip_info_json/') === 0 ) {
        $str=$_SERVER['REQUEST_URI'];
        $prefix='/ip_info_json/';
        if (substr($str, 0, strlen($prefix)) == $prefix) {
          $str = substr($str, strlen($prefix));
        }
        //echo 'SET_TARGET:'.$str;
        set_target($str);
        ip_info_json($str);
        exit;
      }
$valid_tokens=explode (":",getenv('TOKENS'));
//$valid_passwords = array ("testuser" => "testpwd");
//$valid_users = array_keys($valid_passwords);
//$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

$validated=false;

//$user = $_SERVER['PHP_AUTH_USER'];
if(isset($_SERVER['PHP_AUTH_PW'])) {
   $pass = $_SERVER['PHP_AUTH_PW'];
} else {
  $pass="";
}

$authTok="not_set";
if(isset($_POST) && isset($_POST['X-THOMPSON-TOKEN'])) {
$authTok=$_POST['X-THOMPSON-TOKEN'];
}
if($authTok=="not_set" && isset($_GET) && isset($_POST['X-THOMPSON-TOKEN'])) {
$authTok=$_GET['X-THOMPSON-TOKEN'];
}

if($authTok != "not_set") {
  $validated = (in_array($authTok, $valid_tokens));
}

if (!$validated) {
  $validated = (in_array($pass, $valid_tokens));
}
//$ip = getenv('REMOTE_ADDR', true) ?: getenv('REMOTE_ADDR');


if (!$validated) {
  header('WWW-Authenticate: Basic realm="THOMPSON_PING_REALM"');
  header('HTTP/1.0 401 Unauthorized');
  die ("SRY.. but no , and not sorry - Send hostname as Username ,auth string as pass please");
} else {
          if($_SERVER['REQUEST_URI'] == '/ping6' ) {
            ping_ipv6(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/imap6' ) {
            imap_ipv6(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/imaps6' ) {
            imaps_ipv6(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/imap_onion_ssl' ) {
            imap_onion_ssl(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/imap_onion_tls' ) {
            imap_onion_tls(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/imap_onion_insecure' ) {
            imap_onion_insecure(); exit;
          }
          //fallback is tls
          if($_SERVER['REQUEST_URI'] == '/imap_onion' ) {
            imap_onion_tls(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/smtp_onion_insecure' ) {
            smtp_onion('insecure'); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/smtp_onion_tls' ) {
            smtp_onion('tls'); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/smtp_onion_ssl' ) {
            smtp_onion('ssl'); exit;
          }
          //fallback is tls
          if($_SERVER['REQUEST_URI'] == '/smtp_onion' ) {
            smtp_onion('tls'); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/ssh_onion' ) {
            ssh_onion(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/ssh_keyscan_onion' ) {
            ssh_keyscan_onion(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/ssh_weak_onion' ) {
            ssh_weak_onion(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/ssl_weak_nmap' ) {
            ssl_weak_nmap(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/ssl_weak_sslyze' ) {
            ssl_weak_nmap(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/ldaps_onion' ) {
            ldaps_onion(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/ldaps' ) {
            ldaps(); exit;
          }
          if($_SERVER['REQUEST_URI'] == '/ssl_weak_testssl' ) {
            ssl_weak_testssl(); exit;
          }
          echo 'FAIL: NO ROUTE USED ';
          http_response_code(404);

}
