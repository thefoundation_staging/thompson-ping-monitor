FROM webdevops/php-nginx:8.2
#FROM arm64v8/php
#FROM docker.io/tiredofit/nginx-php-fpm:7.4-debian-bookworm

RUN apt-get update && apt-get install -y --no-install-recommends iputils-ping tor supervisor openssh-client curl socat git openssl python3-pretty-yaml && apt-get clean all
#RUN apt-get update && apt install -y bash iputils-ping miredo dnsmasq  tor openssh-client curl socat git python3-pretty-yaml
#RUN /bin/sh -c 'cd;mkdir .ssh;chmod 0600 .ssh ;cd .ssh; ( echo "Host *.onion";echo "  ProxyCommand /usr/bin/socat STDIO SOCKS4A:127.0.0.1:%h:%p,socksport=9050" ) > ~/.ssh/config '
#RUN /bin/sh -c "(echo no-resolv;echo bogus-priv;echo strict-order;echo server=9.9.9.9;echo server=2620:fe::fe;echo server=2606:4700:4700::1111;echo server=1.1.1.1) | tee -a /etc/dnsmasq.conf ; echo nameserver 127.0.0.1 > /etc/resolv.conf||true"

COPY healthcheck /healthcheck
RUN which python || ( ln  -s $(which python3) /usr/bin/python)
#COPY api/.htaccess /app/.htaccess
RUN chmod +x /healthcheck
RUN mkdir /scripts
RUN bash -c "git clone https://github.com/evict/SSHScan.git /scripts/sshscan_evict & git clone https://github.com/GlobalSecurityAgency/sshscan.git /scripts/sshscan & ( cd /scripts/; wget -c https://svn.nmap.org/nmap/scripts/ssl-enum-ciphers.nse ) & wait"
#COPY scripts /scripts
#COPY scripts /
COPY z_setup.sh /.setup.sh
RUN /bin/bash /.setup.sh
#COPY ngx-default.conf /opt/docker/etc/nginx/vhost.common.d/alltoindex.conf
#COPY ngx-default.conf  /opt/docker/etc/nginx/vhost.common.d/10-location-root.conf
#RUN nginx -T
COPY api/index.php /app/index.php
RUN date > /etc/.buildtime
COPY scripts /scripts
COPY init.sh /init.sh
CMD /bin/bash /init.sh
