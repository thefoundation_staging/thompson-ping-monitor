#!/bin/bash 

apt-get update && apt install -y  --no-install-recommends  bash curl git openssl

echo "fork stage"
export DEBIAN_FRONTEND=noninteractive
export DEBIANFRONTEND=noninteractive

apt-get install -y --no-install-recommends iputils-ping miredo python3-setuptools python3-pip dnsmasq jq bsdmainutils openssl tor torsocks openssh-client curl proxychains socat git python3-pretty-yaml dns-root-data ca-certificates nmap  && ( 
    pip3 install --upgrade pip
    pip3 install --upgrade setuptools
    pip3 install --upgrade sslyze ) & 


#nmap-nselibs nmap-scripts #alpine
(sed 's/teredo-debian.remlab.net/teredo.trex.fi/g' -i /etc/miredo.conf) &

( cd;mkdir .ssh;chmod 0600 .ssh ;cd .ssh; ( echo "Host *.onion";echo "  ProxyCommand /usr/bin/socat STDIO SOCKS4A:127.0.0.1:%h:%p,socksport=9050" ) > ~/.ssh/config  ) & 

#curl -s -L https://testssl.sh |tee /scripts/testssl.sh|wc -l && head -n1 /scripts/testssl.sh &
(
    #curl -L https://github.com$(curl https://github.com/drwetter/testssl.sh|grep "archive/refs"|grep href|sed 's/.\+href=//g'|cut -d'"' -f2) > /tmp/.tstssl.zip 
        curl -L https://github.com/drwetter/testssl.sh/archive/refs/tags/$(curl https://github.com/drwetter/testssl.sh/releases|grep expanded_assets|sed 's~.\+github.com/drwetter/testssl.sh/releases/~~'|sed 's/".\+//g'|grep expanded_assets|cut -d/ -f2|head -n1).zip  > /tmp/.tstssl.zip
    cd /scripts
    unzip /tmp/.tstssl.zip 
    mv testssl.sh* testssl
    
    (
        echo '#!/bin/bash'
        echo 'torsocks openssl $@'
    ) > /scripts/openssl
    chmod +x /scripts/openssl
)



(
echo '#!/bin/bash                       
now=$(date +%s)
then=$(cat /tmp/.started)
echo "UPTIME: " $(($now-$then))' > /usr/bin/myuptime
chmod +x /usr/bin/myuptime ) &


wait
( (echo no-resolv;echo bogus-priv;echo strict-order;echo server=9.9.9.9;echo server=2620:fe::fe;echo server=114.114.114.114;echo server=8.8.8.8;echo server=2606:4700:4700::1111;echo server=1.1.1.1) | tee -a /etc/dnsmasq.conf ; echo nameserver 127.0.0.1 > /etc/resolv.conf||true ) & 

#apt-get remove -y git 
#apt-get --autoremove git 
apt-get -y clean all 
cd /var/cache/apt ;find -delete
exit 0 
